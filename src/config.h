#ifndef CAMV_CONFIG_H
#define CAMV_CONFIG_H

/* Default application symbol prefix (e.g. for RND_ACT_* macros) */
#define RND_APP_PREFIX(x)  camv_ ## x

#include "../config.h"

#endif
