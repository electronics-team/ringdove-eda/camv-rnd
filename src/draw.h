#ifndef CAMV_DRAW_H
#define CAMV_DRAW_H

struct rnd_xform_s {
	int dummy;
};

void camv_expose_main(rnd_hid_t *hid, const rnd_hid_expose_ctx_t *region, rnd_xform_t *xform_caller);
void camv_expose_preview(rnd_hid_t *hid, rnd_hid_expose_ctx_t *e);

extern rnd_box_t camv_last_main_expose_region;


/* Temporarily inhibid drawing if this is non-zero. A function that calls a
   lot of other functions that would call camv_draw() a lot in turn may increase
   this value before the calls, then decrease it at the end and call camv_draw().
   This makes sure the whole block is redrawn only once at the end. */
extern rnd_cardinal_t camv_draw_inhibit;

#define camv_draw_inhibit_inc() camv_draw_inhibit++
#define camv_draw_inhibit_dec() \
do { \
	if (camv_draw_inhibit > 0) \
		camv_draw_inhibit--; \
} while(0) \


#endif
