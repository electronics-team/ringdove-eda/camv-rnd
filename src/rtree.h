/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#ifndef CAMV_RTREE_H
#define CAMV_RTREE_H

#include <librnd/core/global_typedefs.h>

typedef long int camv_rtree_cardinal_t;
typedef rnd_coord_t camv_rtree_coord_t;

/* Instantiate an rtree */
#define RTR(n)  camv_rtree_ ## n
#define RTRU(n) CAMV_RTREE_ ## n
#define camv_rtree_privfunc static
#define camv_rtree_size 6
#define camv_rtree_stack_max 8192

#include <genrtree/genrtree_api.h>

#endif /* CAMV_RTREE_H */
