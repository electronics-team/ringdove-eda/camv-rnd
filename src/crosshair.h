#ifndef CAMV_CROSSHAIR_H
#define CAMV_CROSSHAIR_H

#include <librnd/hid/hid.h>

void camv_crosshair_gui_init(void);
void camv_crosshair_gui_uninit(void);

extern rnd_hid_gc_t camv_crosshair_gc;

void camv_hidlib_crosshair_move_to(rnd_design_t *hl, rnd_coord_t abs_x, rnd_coord_t abs_y, int mouse_mot);
void camv_draw_attached(rnd_design_t *hidlib, rnd_bool inhibit_drawing_mode);


#endif
