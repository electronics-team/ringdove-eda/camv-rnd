/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include <librnd/config.h>

#include <assert.h>

#include "obj_any.h"

void camv_obj_add_to_layer(camv_layer_t *ly, camv_any_obj_t *obj)
{
	assert(obj->proto.parent_layer == NULL);
	assert(obj->proto.parent_obj == NULL);
	obj->proto.parent_layer = ly;
	camv_rtree_insert(&ly->objs, obj, camv_obj_bbox(obj));
}


camv_any_obj_t *camv_obj_dup(const camv_any_obj_t *src)
{
	camv_any_obj_t *dst = src->proto.calls->alloc();
	src->proto.calls->copy(dst, src);
	dst->proto.parent_layer = NULL;
	dst->proto.parent_obj = NULL;
	return dst;
}

