extern const char camv_acts_ViewPortDialog[];
extern const char camv_acth_ViewPortDialog[];
fgw_error_t camv_act_ViewPortDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv);

void camv_dlg_viewport_redraw_all(void);

void camv_dlg_viewport_init(void);
void camv_dlg_viewport_uninit(void);

