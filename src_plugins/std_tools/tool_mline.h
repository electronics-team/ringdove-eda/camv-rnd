#ifndef CAMV_TOOL_MLINE
#define CAMV_TOOL_MLINE 1

#include <librnd/config.h>

typedef struct {
	/* statates */
	rnd_coord_t x1, y1, x2, y2;
	int clicked; /* 0 means waiting for x1,y1, 1 means waiting for x2,y2 */
	int valid; /* there is an active attached object line */
} camv_tool_mline_t;

#endif
