#ifndef GCODE_LEX_H
#define GCODE_LEX_H

#include "gcode_vm.h"
#include "gcode.tab.h"

int gcodelex(YYSTYPE *dummy, gcode_prg_t *ctx);
int gcodeerror(gcode_prg_t *ctx, const char *msg);

#endif
