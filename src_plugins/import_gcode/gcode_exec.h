typedef struct {
	void (*travel)(gcode_prg_t *prg, double x1, double y1, double z1, double x2, double y2, double z2);
	void (*linear)(gcode_prg_t *prg, double x1, double y1, double z1, double x2, double y2, double z2);
} gcode_execute_op_t;

int gcode_execute_code(gcode_prg_t *prg, int code, double param);

void gcode_execute_init(gcode_prg_t *prg, const gcode_execute_op_t *ops);
void gcode_execute_uninit(gcode_prg_t *prg);

