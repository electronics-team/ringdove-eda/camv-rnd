typedef struct {
	const struct {
		const struct {
			RND_CFT_BOOLEAN laser;  /* Import g-code assuming laser plotting: M3/M4 is laser on, M5 is laser off */
		} import_gcode;
	} plugins;
} conf_import_gcode_t;

