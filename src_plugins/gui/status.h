#include <librnd/core/event.h>
#include <librnd/core/conf.h>
#include <librnd/core/actions.h>

void camv_status_gui_init_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[]);
void camv_status_st_update_conf(rnd_conf_native_t *cfg, int arr_idx, void *user_data);
void camv_status_rd_update_conf(rnd_conf_native_t *cfg, int arr_idx, void *user_data);
void camv_status_st_update_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[]);
void camv_status_rd_update_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[]);

extern const char camv_acts_StatusSetText[];
extern const char camv_acth_StatusSetText[];
fgw_error_t camv_act_StatusSetText(fgw_arg_t *res, int argc, fgw_arg_t *argv);

